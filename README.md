**Notes on MPS method codebase**

This code contains a few MPS methods for use by the Kolodrubetz group (to be freely shared with attribution). Code is designed for the Ising model with boundary dissipation, but can easily be modified - just look for references to Pauli operators. Methods are based on two references:

1) Review article by Schollwock: https://arxiv.org/abs/1008.3477v2

2) Article on strictly single-site algorithm for DMRG: https://arxiv.org/abs/1501.05504

*TO DO LIST:*

1) Implement code that uses conservation laws.

2) Rewrite S3DMRG to adaptively update mixing rate alpha.

3) Do DMRG pre-conditioning with infinite system DMRG

---

## dmrg_ising.jl

DMRG code. Find ground state of TFI chain using strictly single-site DMRG. Save output as MPS in no particular canonical form.

---

## tebd_ising.jl

Do TEBD. Actually implements a few different methods that can be mixed and matched as needed:

1) Imaginary time evolution exp(-tau*H) on wave function. Note that this will not maintain the norm or canonicalization of the MPS.

2) Real time evolution of density matrix stored as MPS (MPO with bra and ket combined into superindex)

3) Real time Heisenberg evolution of operator stored as MPS (MPO with bra and ket combined into superindex)

---

## tebd_ising_boundary_diss.jl

Special implementation of TEBD on density matrix with boundary dissipation, where MPO has been unfolded using a mapping similar to arXiv:1812.10373. Details of the technique can be found in notes_dissipative_tebd.pdf.

---

## notes_dissipative_tebd.pdf

Notes with various details on implementation of methods.

