using PyPlot
using LinearAlgebra
using NPZ

# Run TEBD
include("tebd_ising.jl")

# Run ED
function get_sz(u,site)
    return ((u-1) & (1<<(site-1))==0) ? 1 : -1 # u-1 gives binary in range 0 to 2^L-1
end

function flip_site(u,site)
    return xor(u-1,1<<(site-1))+1
end

include("params_tebd_ising.jl")
basis_sz=2^L
H=zeros(basis_sz,basis_sz)
x_site=zeros(L,basis_sz,basis_sz)
z_site=zeros(L,basis_sz,basis_sz)

for u in 1:basis_sz
    for site in 1:L
        if site != L
            H[u,u]-=jz*get_sz(u,site)*get_sz(u,site+1)
        end
        u2=flip_site(u,site)
        if u2 < 1 || u2 > basis_sz
            println("issue finding u2 for u=",u," or ",bitstring(u)[end-L+1:end],", site=",site,", u2=",u2," or ",bitstring(u2)[end-L+1:end])
            exit()
        end
        H[u,u2]=-h
        x_site[site,u,u2]=1
        z_site[site,u,u]=get_sz(u,site)
    end
end

L_z=sqrt(gamma)*reshape(z_site[1,:,:],basis_sz,basis_sz) # Lindblad operator on boundary

if init_dir=='x'
    psi=ones(ComplexF64,basis_sz)
elseif init_dir=='z'
    psi=zeros(ComplexF64,basis_sz)
    psi[1]=1
else
    println("Invalid initial direction: ",init_dir)
    exit()
end
psi=psi/sqrt(psi'*psi)
U=exp(-im*dt*H)
V=exp(-dt*H)

t_ed_prep=collect(0:dt:(n_steps_imag_time_ev+0.5)*dt)

sx_ed_prep=zeros(L,n_steps_imag_time_ev+1)
sz_ed_prep=zeros(L,n_steps_imag_time_ev+1)

for site in 1:L
    sx_ed_prep[site,1]=real(psi'*reshape(x_site[site,:,:],(basis_sz,basis_sz))*psi)
    sz_ed_prep[site,1]=real(psi'*reshape(z_site[site,:,:],(basis_sz,basis_sz))*psi)
end

for j in 1:n_steps_imag_time_ev
    global psi
    psi=V*psi
    psi=psi/norm(psi)
    for site in 1:L
        sx_ed_prep[site,j+1]=real(psi'*reshape(x_site[site,:,:],(basis_sz,basis_sz))*psi)
        sz_ed_prep[site,j+1]=real(psi'*reshape(z_site[site,:,:],(basis_sz,basis_sz))*psi)
    end
end

# # Time evolve with psi(t)=exp(-i*t*H) psi(0)
# sx_ed=zeros(L,n_steps_time_ev+1)

# for site in 1:L
#     sx_ed[site,1]=real(psi'*reshape(x_site[site,:,:],(basis_sz,basis_sz))*psi)
# end

# for j in 1:n_steps_time_ev
#     global psi
#     psi=U*psi
#     for site in 1:L
#         sx_ed[site,j+1]=real(psi'*reshape(x_site[site,:,:],(basis_sz,basis_sz))*psi)
#     end
# end

# Time evolve with Lindblad

Dt_heis=npzread("Dt.npy")

sx_ed=zeros(L,n_steps_time_ev+1)
sz_ed=zeros(L,n_steps_time_ev+1)
C_ed=zeros(length(Dt_heis),n_steps_time_ev+1)

t_ed=collect(0:dt:(n_steps_time_ev+0.5)*dt)

rho=reshape(psi*psi',basis_sz,basis_sz)

using DifferentialEquations

function drho_dt(rho,p,t)
    return -im*H*rho+im*rho*H+L_z*rho*L_z-0.5*(L_z*L_z*rho + rho*L_z*L_z)
end

tspan = (0.0,t_ed[end])
prob = ODEProblem(drho_dt,rho,tspan)
println("Solving ODE")
sol = solve(prob, Tsit5(), reltol=1e-8, abstol=1e-8)

# for site in 1:L
#     sx_ed[site,1]=real(tr(rho*reshape(x_site[site,:,:],(basis_sz,basis_sz))))
#     sz_ed[site,1]=real(tr(rho*reshape(z_site[site,:,:],(basis_sz,basis_sz))))
# end

for j in 1:length(t_ed)
    println("Evaluating at step ",j," out of ",n_steps_time_ev)
    rho=sol(t_ed[j])
    for site in 1:L
        sx_ed[site,j]=real(tr(rho*reshape(x_site[site,:,:],(basis_sz,basis_sz))))
        sz_ed[site,j]=real(tr(rho*reshape(z_site[site,:,:],(basis_sz,basis_sz))))
    end

    sz_times_rho=reshape(z_site[1,:,:],(basis_sz,basis_sz))*rho
    tspan_heis = (0.0,Dt_heis[end])
    prob_heis = ODEProblem(drho_dt,sz_times_rho,tspan_heis)
    println("Solving Heisenberg ODE")
    sol_heis = solve(prob_heis, Tsit5(), reltol=1e-8, abstol=1e-8)
    for k in 1:length(Dt_heis)
        sz_times_rho=sol_heis(Dt_heis[k])
        C_ed[k,j]=real(tr(sz_times_rho*reshape(z_site[1,:,:],(basis_sz,basis_sz))))
    end
end

npzwrite("t_ed_prep.npy",t_ed_prep)
npzwrite("sx_ed_prep.npy",sx_ed_prep)
npzwrite("sz_ed_prep.npy",sz_ed_prep)
npzwrite("t_ed.npy",t_ed)
npzwrite("sx_ed.npy",sx_ed)
npzwrite("sz_ed.npy",sz_ed)
npzwrite("C_ed.npy",C_ed)

# Import TEBD data

norm_tebd_prep=npzread("norm_prep.npy")
sx_tebd_prep=npzread("sx_prep.npy")
sz_tebd_prep=npzread("sz_prep.npy")
mps_norm_tebd=npzread("mps_norm.npy")
mpo_norm_tebd=npzread("mpo_norm.npy")
sx_tebd=npzread("sx.npy")
sz_tebd=npzread("sz.npy")
C_tebd=npzread("C.npy")

#site=1#Int64(round(L/2))

println("Max diff of sx, imag time ev = ",maximum(abs.(sx_ed_prep-sx_tebd_prep)))
println("Max diff of sx, time ev = ",maximum(abs.(sx_ed-sx_tebd)))
println("Max diff of sz, imag time ev = ",maximum(abs.(sz_ed_prep-sz_tebd_prep)))
println("Max diff of sz, time ev = ",maximum(abs.(sz_ed-sz_tebd)))
println("Max diff of C = ",maximum(abs.(C_ed-C_tebd)))

ind=Int64(round(size(C_ed,2)/2))
plot(Dt_heis,C_ed[:,ind],label="ED")
plot(Dt_heis,C_tebd[:,ind],label="TEBD")
legend(loc="best")

# for site in 1:L
#     figure(site)
#     plot(t_ed_prep,sx_ed_prep[site,:],"b",label="sx(site "*string(site)*") ED")
#     plot(t_ed.+t_ed_prep[end],sx_ed[site,:],"b")
#     plot(t_ed_prep,sx_tebd_prep[site,:],"r",label="sx(site "*string(site)*") TEBD")
#     plot(t_ed.+t_ed_prep[end],sx_tebd[site,:],"r")
#     legend(loc="best")
# end
show()

# figure(2)
# plot(t_ed_prep,sz_ed_prep[site,:],"b",label="sz(site "*string(site)*") ED")
# plot(t_ed.+t_ed_prep[end],sz_ed[site,:],"b")
# plot(t_ed_prep,sz_tebd_prep[site,:],"r",label="sz(site "*string(site)*") TEBD")
# plot(t_ed.+t_ed_prep[end],sz_tebd[site,:],"r")
# legend(loc="best")
# show()

# figure(2)
# plot(t_ed_prep,norm_tebd_prep,label="MPS norm, imag time ev")
# plot(t_ed.+t_ed_prep[end],mps_norm_tebd,label="MPS norm, real time ev")
# plot(t_ed.+t_ed_prep[end],mpo_norm_tebd,label="MPO norm, real time ev")
# legend(loc="best")
# show()
