# Find ground state of TFI chain using DMRG
# Doing strictly single-site DMRG (S3DMRG) as spelled out in arXiv:1501.05504v2

using LinearAlgebra
using TensorOperations
using NPZ
using SparseArrays
using Arpack
using LinearMaps

pauli=[[0 1;1 0],[0 -im;im 0],[1 0;0 -1],[1 0;0 1]]

"""
svd_and_trim!(m,epsilon,chi)

Do SVD m = a * Diagonal(s) * v', cut to min of error epsilon on sqrt(sum(s^2)) or max bond dim chi.
To conserve memory, use svd!, meaning that m is trash after running it.

Note that if it is run with chi=0, will not trim.
"""
function svd_and_trim!(m,epsilon,chi)
    F=svd!(m) # m is now garbage
    a=F.U
    s=F.S
    vdag=F.Vt

    if chi==0
        return (a,s,vdag)
    end
    
    error = 0
    dim = 1
    for k in 0:(length(s)-1)
	error += s[end-k]^2
	if sqrt(error) > epsilon
	    dim = length(s)-k
	    break
        end
    end
    
    dim = min(dim,chi)
    
    return (a[:,1:dim], s[1:dim], vdag[1:dim,:])
end

"""
calc_norm(B)

Calculate the norm, <psi|psi>, of wfn psi stores as right-canonical MPS B.
"""
function calc_norm(B)
    # Contract from left to right (shouldn't matter, but note that this is oppose the canonical form
    C=ones(1,1) # Contracted matrix
    for site in 1:L
        @tensoropt C_temp[a2,a2p] := C[a1,a1p] * B[site][s1,a1,a2] * conj(B[site][s1,a1p,a2p])
        C=deepcopy(C_temp)
    end
    return C[1,1]
end

"""
calc_pauli(B, p, psite)

Calculate the expectation of a Pauli matrix on given site, <psi|sigma^p_site|psi>, of wfn psi stored as right-canonical MPS B.
"""
function calc_pauli(B, p, psite)
    # Contract from left to right
    C=ones(1,1) # Contracted matrix
    for site in 1:L
        if site == psite
            @tensoropt C_temp[a2,a2p] := C[a1,a1p] * B[site][s1,a1,a2] * pauli[p][s1p,s1] *conj(B[site][s1p,a1p,a2p])
        else
            @tensoropt C_temp[a2,a2p] := C[a1,a1p] * B[site][s1,a1,a2] * conj(B[site][s1,a1p,a2p])
        end
        C=deepcopy(C_temp)
    end
    return real(C[1,1])
end

"""
parse_index(ind, ind_sz)

Convert index ind to multi-dimensional index of size ind_sz. 
Should work such that reshape(A[ind_out]=1,product(ind_sz)) gives A[ind]=1

NOTE: THIS HAS BEEN CHECKED FOR EACH INDEX WITH ind_sz=(3,4,5)
"""
function parse_index(ind, ind_sz)
    val=ind-1
    ind_out=zeros(Int64,length(ind_sz))
    for j in 1:length(ind_sz)
        ind_out[j]=val % ind_sz[j] + 1
        val=div(val,ind_sz[j])
    end
    return Tuple(ind_out)
end

"""
hash_index(ind_tup, ind_sz)

Convert multi-dimensional index tuple ind_tup to single index.
Inverse of parse_index, i.e., should be that ind_tup == parse_index(hash_index(ind_tup,ind_sz),ind_sz)

NOTE: THIS HAS BEEN CHECKED FOR EACH INDEX WITH ind_sz=(3,4,5)
"""
function hash_index(ind_tup, ind_sz)
    ind=0
    for j in length(ind_sz):-1:1
        ind*=ind_sz[j]
        ind+=(ind_tup[j]-1)
    end
    return ind + 1
end

"""
apply_H_eff!(vf,v,L_left,H_sparse,R_right)

Apply effective Hamiltonian on vector v, where H_eff is defined through
H_eff[(s1,a1,a2),(s2,c1,c2)] := L_left[a1,b1,c1] * H_sparse[s1,s2][b1,b2] * R_right[a2,b2,c2]
with H_sparse as a sparse matrix.
"""
function apply_H_eff!(vf,v,L_left,H_sparse,R_right)
    fill!(vf,0)
    chi_left=size(L_left,1)
    chi_right=size(R_right,1)
    ind_sz=(2,chi_left,chi_right) # For hashing (s1,a1,a2) <-> s1,a1,a2
    
    for j in 1:length(v)
        (s2,c1,c2)=parse_index(j,ind_sz)
        for s1 in 1:2
            temp=reshape(L_left[:,:,c1]*H_sparse[s1,s2]*transpose(R_right[:,:,c2]),chi_left*chi_right) # temp[(a1,a2)]
            vf[s1:2:length(vf)].+=v[j]*temp
        end
    end
end

"""
dmrg!(mps,H_mpo_sparse,H_mpo_dense,L_mat,R_mat,chi,dir)

Do single DMRG sweep in mps using Hamiltonian stored as MPO, fixed bond dimension chi, dir=0 (left to right) or 1 (right to left).

To minimize recalculation, reuse and update L_mat and R_mat from previous iterations, 
which are the contracted versions of <psi|H|psi>.

MPS is assumed to be in appropriate canonical form and to have bond dimension chi.
"""
function dmrg!(mps,H_mpo_sparse,H_mpo_dense,L_mat,R_mat,chi,dir,alpha) # ***Note to self - later want alpha to be adaptively found***
    LR_end=ones(1,1,1)
    site_start=(dir==0) ? 1 : L
    site_end=(dir==0) ? L : 1
    step=(dir==0) ? 1 : -1
    for site in site_start:step:site_end
        if site==1
            L_left=LR_end
            R_right=R_mat[site+1]
        elseif site==L
            L_left=L_mat[site-1]
            R_right=LR_end
        else
            L_left=L_mat[site-1]
            R_right=R_mat[site+1]
        end
        # Alias to avoid iterating in @tensoropt
        H_curr_sparse=H_mpo_sparse[site]
        H_curr_dense=H_mpo_dense[site]
        mps_curr=mps[site]
        
        # Current bond dimensions of left and right matrices
        chi_left=size(L_left,1)
        chi_right=size(R_right,1)
        @assert chi_left==size(L_left,3)
        @assert chi_right==size(R_right,3)
        H_dim=2*chi_left*chi_right
        H_dim_sz=(2,chi_left,chi_right)
        
        mytype=(eltype(L_left)==ComplexF64 || eltype(H_curr_sparse[1,1])==ComplexF64
                || eltype(R_right)==ComplexF64) ? ComplexF64 : Float64 # Determine element type of linear map
        sym = (mytype==Float64) # If real matrix, is symmetric. If complex, is not
        M = LinearMap{mytype}((vf,v)->apply_H_eff!(vf,v,L_left,H_curr_sparse,R_right), H_dim;
                              ismutating=true, ishermitian=true, issymmetric=sym)
        (E_opt,gs_eff)=eigs(M,nev=1,which=:SR,v0=reshape(mps_curr,H_dim))

        M_opt=reshape(gs_eff,size(mps_curr)) # This is best guess, but need to do basis expansion for strictly single-site (S3) DMRG

        if dir==0
            if site==L 
                mps[site]=deepcopy(M_opt)
            else # Do subspace expansion
                @tensoropt P_site[s2,c1,a2,b2] := alpha*L_left[a1,b1,c1]*M_opt[s1,a1,a2]*H_curr_dense[s2,s1,b1,b2]
                P_site=reshape(P_site,2,chi_left,chi_right*size(H_curr_dense,4)) # Reshape to P_site[s2,c1,(a2,b2)]
                # M_opt is expanded to M_exp = [M_opt P_site] horizontally (dimension 3)
                M_exp = cat(M_opt,P_site,dims=(3))
                
                # Reshape as [(s1,a1),a2], trim down to bond dimension chi
                (U,S,V_dag)=svd_and_trim!(reshape(M_exp,size(M_exp,1)*size(M_exp,2),size(M_exp,3)),0,chi)
                    
                # Update mps[site]
                mps[site]=reshape(U,size(mps_curr,1),size(mps_curr,2),size(U,2)) # Reshape to [s1,a1,a2]
                
                # Update mps[site+1]
                # mps[site+1] is expanded vertically to [mps[site+1]; 0] to enable mutliplication by M_exp
                # at this point MPS represents same wave function. Call this B_temp
                B_temp=cat(mps[site+1],zeros(size(mps[site+1],1),size(P_site,3),size(mps[site+1],3)),dims=(2))
                # println("Before @tensoropt, typeof(mps[site+1])=",typeof(mps[site+1]),", typeof(Diagonal(S))=",typeof(Diagonal(S)),
                #         ", typeof(V_dag)=",typeof(V_dag),", typeof(B_temp)=",typeof(B_temp))
                @tensoropt mps[site+1][s2,a1,a2]:=V_dag[a1p,a1pp]*Diagonal(S)[a1,a1p]*B_temp[s2,a1pp,a2] # For some reason, needs to be in this order to avoid type error
            end
                
            # Update L_mat[site] matrices
            mps_curr=mps[site] # Re-alias to updated mps
            @tensoropt L_mat[site][a1,b1,c1] := L_left[a0,b0,c0]*mps_curr[s1,a0,a1]*H_curr_dense[s1p,s1,b0,b1]*conj(mps_curr[s1p,c0,c1])
        else
            if site==1
                mps[site]=deepcopy(M_opt)
            else # Do subspace expansion
                @tensoropt P_site[s2,a1,b1,c2] := alpha*R_right[a2,b2,c2]*M_opt[s1,a1,a2]*H_curr_dense[s2,s1,b1,b2]
                P_site=reshape(P_site,2,chi_left*size(H_curr_dense,3),chi_right) # Reshape to P_site[s2,(a1,b1),c2]
                # M_opt is expanded to M_exp = [M_opt; P_site] vertically (dimension 2)
                M_exp = cat(M_opt,P_site,dims=(2))

                # Reshape [s2,a1,a2] -> [a1,(s2,a2)], trim down to bond dimension chi
                (U,S,V_dag)=svd_and_trim!(reshape(permutedims(M_exp,(2,1,3)),size(M_exp,2),size(M_exp,1)*size(M_exp,3)),0,chi)
                
                # Update mps[site]
                # Reshape [a1,(s2,a2)] -> [s2,a1,a2]
                mps[site]=permutedims(reshape(V_dag,size(V_dag,1),size(mps_curr,1),size(mps_curr,3)),(2,1,3))
                
                # Update mps[site-1]
                # mps[site-1] is expanded horizontally to [mps[site-1] 0] to enable mutliplication by M_exp
                A_temp=cat(mps[site-1],zeros(size(mps[site-1],1),size(mps[site-1],2),size(P_site,2)),dims=(3))
                @tensoropt mps[site-1][s2,a1,a2]:=A_temp[s2,a1,a2p]*U[a2p,a2pp]*Diagonal(S)[a2pp,a2]
            end
            
            # Update R_mat[site] matrices
            mps_curr=mps[site] # Re-alias to updated mps
            @tensoropt R_mat[site][a1,b1,c1] := R_right[a2,b2,c2]*mps_curr[s1,a1,a2]*H_curr_dense[s1p,s1,b1,b2]*conj(mps_curr[s1p,c1,c2])
        end
    end # for loop over sites
end # function dmrg!

"""
create_H_mpo!(H_mpo_dense,H_mpo_sparse,L,h,J_z)

Initialize Hamiltonian H=sum_j[-h*s^x_j - J*s^z_j*s^z_{j+1}] as sparse matrix MPO with bond dim 3.
Use construction from Schollwock review, with bulk matrices
M = [I 0 0; s_z 0 0; -h*s_x -J*s_z I]

For later use, each site MPO will be stored in two different ways:
1) as a rank-2 array of sparse matrices: H_mpo_sparse[site][s1,s2][a1,a2]
1) as a rank-4 (dense) tensor: H_mpo_dense[site][s1,s2,a1,a2]
"""
function create_H_mpo!(H_mpo_dense,H_mpo_sparse,L,h,J_z)
    resize!(H_mpo_dense,0)
    resize!(H_mpo_sparse,0)
    mpo_sz=(2,2,3,3)
    M_0=zeros(mpo_sz) # Bulk MPO matrix stored as [s1,s2,a1,a2]
    M_0[:,:,1,1]=pauli[4]
    M_0[:,:,2,1]=pauli[3]
    M_0[:,:,3,1]=-h*pauli[1]
    M_0[:,:,3,2]=-J_z*pauli[3]
    M_0[:,:,3,3]=pauli[4]

    S_0=Array{Any,2}(undef,2,2)
                                  
    for site in 1:L
        for s1 in 1:2
            for s2 in 1:2
                if site==1
                    S_0[s1,s2]=sparse(M_0[s1,s2,3:3,:])
                elseif site==L
                    S_0[s1,s2]=sparse(M_0[s1,s2,:,1:1])
                else
                    S_0[s1,s2]=sparse(M_0[s1,s2,:,:])
                end
            end
        end
        push!(H_mpo_sparse,deepcopy(S_0))
        if site==1
            push!(H_mpo_dense,deepcopy(M_0[:,:,3:3,:]))
        elseif site==L
            push!(H_mpo_dense,deepcopy(M_0[:,:,:,1:1]))
        else
            push!(H_mpo_dense,deepcopy(M_0[:,:,:,:]))
        end
    end
end # function create_H_mpo!

"""
exp_val(mpo,mps)

Calculate <mps|mpo|mps>. Note that, for H_mpo, this is already available in L or R matrices,
but useful to be able to recalculate as double-check.
"""
function exp_val(mpo,mps)
    @assert length(mpo)==length(mps)
    C=ones(1,1,1) # Contracted matrix
    for site in 1:length(mps)
        mps_curr=mps[site]
        mpo_curr=mpo[site]
        @tensoropt C_temp[a2,b2,c2]:=mps_curr[s1,a1,a2]*mpo_curr[s2,s1,b1,b2]*conj(mps_curr[s2,c1,c2])*C[a1,b1,c1]
        C=deepcopy(C_temp)
    end
    return C[1,1,1]
end

function main()

    include("params_dmrg_ising.jl")

        
    # Start by preparing the initial states as an MPS
    psi_mps = []
    M_0=zeros(2,1,1) # Initialize MPS as bond dimension 1 to ensure left&right canonical form
    if init_dir=='x'
        # Start with everything pointing to the right, spin wfn (1/sqrt(2),1/sqrt(2))
        M_0[1,1,1]=1/sqrt(2) 
        M_0[2,1,1]=1/sqrt(2)
    elseif init_dir=='z'
        # Start with everything pointing up, spin wfn (1,0)
        M_0[1,1,1]=1
    else
        println("Invalid initial direction: ",init_dir)
        exit()
    end

    for i in 1:L
	push!(psi_mps,deepcopy(M_0))
    end

    # Construct Hamiltonian as normal MPO and sparse MPO
    H_mpo_dense=[]
    H_mpo_sparse=[]
    create_H_mpo!(H_mpo_dense,H_mpo_sparse,L,h,J_z)
    
    # Initialize L and R matrices
    L_mat=[]
    R_mat=[]
    L_prev=ones(1,1,1) # L_0=1 as rank-3 tensor
    for site in 1:L
        @tensoropt L_curr[a1,b1,c1]:=L_prev[a0,b0,c0]*psi_mps[site][s1,a0,a1]*H_mpo_dense[site][s2,s1,b0,b1]*conj(psi_mps[site][s2,c0,c1])
        L_prev=deepcopy(L_curr)
        push!(L_mat,L_prev)
    end
    R_prev=ones(1,1,1) # R_L=1 as rank-3 tensor
    for site in L:-1:1
        @tensoropt R_curr[a0,b0,c0] := R_prev[a1,b1,c1]*psi_mps[site][s1,a0,a1]*H_mpo_dense[site][s2,s1,b0,b1]*conj(psi_mps[site][s2,c0,c1])
        R_prev=deepcopy(R_curr)
        push!(R_mat,R_prev)
    end
    reverse!(R_mat)

    E=zeros(2*length(1:2:n_sweeps))

        
    # Sweep
    for sweep in 1:2:n_sweeps
        alpha=alpha_init*exp(-(sweep-1)/alpha_rate)
        println("Starting sweep ",sweep)
        dmrg!(psi_mps,H_mpo_sparse,H_mpo_dense,L_mat,R_mat,chi,1,alpha) # Sweep right to left
        E[sweep]=real(exp_val(H_mpo_dense,psi_mps))

        alpha=alpha_init*exp(-(sweep)/alpha_rate)
        println("Starting sweep ",sweep+1)
        dmrg!(psi_mps,H_mpo_sparse,H_mpo_dense,L_mat,R_mat,chi,0,alpha) # Sweep left to right
        E[sweep+1]=real(exp_val(H_mpo_dense,psi_mps))
    end

    npzwrite("E.npy",E)
    
    for site in 1:L
        npzwrite("mps_"*string(site)*".npy",psi_mps[site])
    end
end

main()
