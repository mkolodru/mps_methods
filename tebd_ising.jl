# Find time evolution of TFI chain using TEBD
# Using notations from Schollwock review, arXiv:1008.3477v2
# Code is current written to do three things (in order):
#     1) Do imaginary time evolution exp(-t*H) to push towards ground state. Can easily be modified for real time evolution of wfn.
#     2) Do real time evolution in Heisenberg picture of sz at the boundary, using same Hamiltonian, along with boundary Lindblad operator sqrt(gamma)*sz.
#     3) Do real time evolution of density matrix with same Hamiltonian, along with boundary Lindblad operator sqrt(gamma)*sz.

using LinearAlgebra
using TensorOperations
using NPZ

pauli=[[0 1;1 0],[0 -im;im 0],[1 0;0 -1],[1 0;0 1]]

"""
make_unitary(jz,h1,h2,dt)

Create unitary U = exp(-i*H*dt) for TEBD of the 2-site Hamiltonian H=-jz*z1*z2 - h1*x1 - h2*x2.
Return the unitary as a 2x2x2x2 tensor in the form needed for TEBD, U[s1,s2,s1',s2']
"""
function make_unitary(jz,h1,h2,dt)
    @tensor twositeham[s1,s2,s1p,s2p] := (-jz*pauli[3][s1,s1p]*pauli[3][s2,s2p]
                                          -h1*pauli[1][s1,s1p]*pauli[4][s2,s2p]
                                          -h2*pauli[4][s1,s1p]*pauli[1][s2,s2p])

    return reshape(exp(-im*dt*reshape(twositeham,(4,4))),(2,2,2,2)) # u[s1,s2,s1',s2']*psi[s1',s2']=psif[s1,s2]
end

"""
make_imag_time_ev(jz,h1,h2,dt)

Create imaginary time evolution operator V = exp(-H*dt) for TEBD of the 2-site Hamiltonian H=-jz*z1*z2 - h1*x1 - h2*x2.
Return the operator as a 2x2x2x2 tensor in the form needed for TEBD, U[s1,s2,s1',s2']
"""
function make_imag_time_ev(jz,h1,h2,dt)
    return make_unitary(jz,h1,h2,-im*dt)
end

"""
make_rho_mps_from_psi_mps(B_psi, Lam_psi)

Create MPS corresponding to density matrix rho = |psi><psi| in TEBD form (right-normd B matrices and diagonal Lambda matrices)
"""
function make_rho_mps_from_psi_mps(B_psi, Lam_psi)
    B_rho=[]
    Lam_rho=[]
    L=length(B_psi)
    for site in 1:L
        @tensor B[s1,s1p,a0,a0p,a1,a1p] := B_psi[site][s1,a0,a1]*conj(B_psi[site][s1p,a0p,a1p]) # Define B as MPO
        # Reshape MPO to MPS, [s1,s1p,a0,a0p,a1,a1p] -> [(s1,s1p),(a0,a0p),(a1,a1p)], where "primed" parts, e.g. s1p, correspond to bra
        push!(B_rho,reshape(B,size(B_psi[site],1)^2,size(B_psi[site],2)^2,size(B_psi[site],3)^2)) 

        if site < L
            lsz=size(Lam_psi[site],1)
            @assert size(Lam_psi[site],1)==size(Lam_psi[site],2) # Sanity check Lam_psi is stored as a diagonal matrix
            Lam_diag=zeros(lsz,lsz)
            for a in 1:size(Lam_psi[site],1)
                for ap in 1:size(Lam_psi[site],1)
                    Lam_diag[a,ap]=Lam_psi[site][a,a]*Lam_psi[site][ap,ap]
                end
            end
            push!(Lam_rho,Diagonal(reshape(Lam_diag,lsz^2))) # Reshape to superindex [a,ap] -> [(a,ap)], then store as diagonal matrix
        end
    end

    return (B_rho,Lam_rho)
end

"""
make_lindblad_time_ev(jz,h1,h2,gamma,dt)

Create Lindblad time evolution operator U = exp(LL*dt) for TEBD of the 2-site Hamiltonian H=-jz*z1*z2 - h1*x1 - h2*x2
with a single Lindblad operator L = sqrt(gamma)*z1.

Return the operator as a 4D tensor in the form needed for TEBD, U[(s1,s1'),(s2,s2'),(s1'',s1'''),(s2'',s2''')]
"""
function make_lindblad_time_ev(jz,h1,h2,gamma,dt)
    # Define 2-site Hamiltonian
    @tensor H[s1,s2,s1p,s2p] := -jz*pauli[3][s1,s1p]*pauli[3][s2,s2p] - h1*pauli[1][s1,s1p]*pauli[4][s2,s2p] - h2*pauli[4][s1,s1p]*pauli[1][s2,s2p]

    # Define 2-site Lindblad operator
    @tensor L[s1,s2,s1p,s2p] := sqrt(gamma)*pauli[3][s1,s1p]*pauli[4][s2,s2p]

    # AA is superoperator corresponding to -i*H*rho and could be modified to include -0.5*L^\dagger*L*rho (which is just an identity for our L)
    @tensor AA[s1,s1p,s2,s2p,s1pp,s1ppp,s2pp,s2ppp] := -im*H[s1,s2,s1pp,s2pp]*pauli[4][s1p,s1ppp]*pauli[4][s2p,s2ppp]

    # BB is superoperator corresponding to i*rho*H
    @tensor BB[s1,s1p,s2,s2p,s1pp,s1ppp,s2pp,s2ppp] := im*H[s1ppp,s2ppp,s1p,s2p]*pauli[4][s1,s1pp]*pauli[4][s2,s2pp]

    # CC is superoperator corresponding to L*rho*L^\dagger
    @tensor CC[s1,s1p,s2,s2p,s1pp,s1ppp,s2pp,s2ppp] := L[s1,s2,s1pp,s2pp]*L[s1ppp,s2ppp,s1p,s2p]

    LL=reshape(AA+BB+CC,(16,16))-gamma*Matrix(I,16,16) # Liovillean superop, reshaped as LL[(s1,s1',s2,s2'),(s1'',s1''',s2'',s2''')], adding back in gamma*I term

    # return U s.t. U[(s1,s1'),(s2,s2'),(s1'',s1'''),(s2'',s2''')]*rho(t)[(s1'',s1'''),(s2'',s2''')]=rho(t+dt)[(s1,s1'),(s2,s2')]
    return reshape(exp(dt*LL),(4,4,4,4)) 
end

"""
svd_and_trim!(m,epsilon,chi)

Do SVD m = a * Diagonal(s) * v', cut to min of error epsilon on sqrt(sum(s^2)) or max bond dim chi.
To conserve memory, use svd!, meaning that m is trash after running it.
"""
function svd_and_trim!(m,epsilon,chi)
    F=svd!(m) # m is now garbage
    a=F.U
    s=F.S
    vdag=F.Vt

    error = 0
    dim = 1
    for k in 0:(length(s)-1)
	error += s[end-k]^2
	if sqrt(error) > epsilon
	    dim = length(s)-k
	    break
        end
    end
    
    dim = min(dim,chi)
    
    return (a[:,1:dim], s[1:dim], vdag[1:dim,:])
end

"""
tedb!(B,Lam,U_bulk,U_left,U_right,chi,epsilon,bond,renorm=true)

Do TEBD on density matrix stored as site matrices B, bond matrices Lam, unitary time evolution U_bulk, 
U_left, and U_right, max bond dimension chi, max error epsilon. bond=0 or 1 specifies even or odd bonds.

If renorm is set to false, does not divide by sum of squared singular values (norm) on truncation.
"""
function tedb!(B,Lam,U_bulk,U_left,U_right,chi,epsilon,bond,renorm=true)
    L=length(B) # Number of sites
    startsite = (bond == 0) ? 1 : 2
    endsite = L-1
    one_tensor=ones(1,1) # For use at the edge, 2-dimensional tensor Lam_0 = Lam_L = 1.
    a=nothing
    s=nothing
    vdag=nothing
    
    # unitary evolution on all even/odd bonds
    for i in startsite:2:endsite
        # Pick the correct unitary for evolution (as alias to U_curr)
        U_curr=U_bulk
        if i==1 
            U_curr=U_left
        end
        if i==L-1 && i!=1
            U_curr=U_right
        end

        # Lam[i-1] = L_0 iff i=1
        Lam_prev=(i==1) ? one_tensor : Lam[i-1]

        # Do TEBD evolution under U_curr, using notation from Schollwock review
        @tensor psi_bar[s1,s2,a0,a2]:=B[i][s1,a0,a1]*B[i+1][s2,a1,a2]
	@tensor phi_bar[s1,s2,a0,a2] := U_curr[s1,s2,s1p,s2p]*psi_bar[s1p,s2p,a0,a2]
	@tensor phi[a0,s1,a2,s2] := Lam_prev[a0,a1]*phi_bar[s1,s2,a1,a2]
        phi=reshape(phi,size(phi,1)*size(phi,2),size(phi,3)*size(phi,4)) # Regroup as phi[(a0,s1),(a2,s2)]

	(a,s,vdag) = svd_and_trim!(phi,epsilon,chi) # phi is now garbage

        norm = renorm ? sqrt(sum(s.^2)) : 1
	Lam[i] = Diagonal(s)./norm # Stored as sparse diagonal matrix for simplicity
	
        B[i+1] = permutedims(reshape(vdag,length(s),size(phi_bar,4),size(phi_bar,1)),(3,1,2)) # reshape from [a1,(a2,s2)] to [a1,a2,s2], then permute to [s2,a1,a2]

	# While not notes in Schollwock, the equation B[i] = phi_bar*B[i+1]^\dagger only hold without truncation.
        # If we truncate Lam[i], dividing it by norm, this renormalizes phi_bar = Gamma[i]*Lam[i]*Gamma[i+1]*Lam[i+1] as well
	@tensor B[i][s1,a0,a1] := phi_bar[s1,s2,a0,a2]*conj(B[i+1][s2,a1,a2])/norm 
    end # for i in startsite:2:endsite
end # function tebd

"""
calc_norm(B)

Calculate the norm, <psi|psi>, of wfn psi stored as right-canonical MPS B.
"""
function calc_norm(B)
    # Contract from left to right
    C=ones(1,1) # Contracted matrix
    for site in 1:L
        @tensoropt C_temp[a2,a2p] := C[a1,a1p] * B[site][s1,a1,a2] * conj(B[site][s1,a1p,a2p])
        C=copy(C_temp)
    end
    return C[1,1]
end

"""
calc_pauli(B, p, psite)

Calculate the expectation of a Pauli matrix on given site, <psi|sigma^p_site|psi>, of wfn psi stored as right-canonical MPS B.
"""
function calc_pauli(B, p, psite)
    # Contract from left to right
    C=ones(1,1) # Contracted matrix
    for site in 1:L
        if site == psite
            @tensoropt C_temp[a2,a2p] := C[a1,a1p] * B[site][s1,a1,a2] * pauli[p][s1p,s1] *conj(B[site][s1p,a1p,a2p])
        else
            @tensoropt C_temp[a2,a2p] := C[a1,a1p] * B[site][s1,a1,a2] * conj(B[site][s1,a1p,a2p])
        end
        C=copy(C_temp)
    end
    return real(C[1,1])
end
	
"""
calc_norm_rho(B)

Calculate the norm, Tr(rho), of density matrix stored as right-canonical MPS B.
"""
function calc_norm_rho(B)
    C=ones(1) # Contracted matrix
    for site in 1:L
        B_mpo=reshape(B[site],2,2,size(B[site],2),size(B[site],3))
        @tensoropt C_temp[a2] := C[a1] * B_mpo[s1,s1,a1,a2] # Do trace and tensor multiplication. Could also do as matrix multiplication, add element 1 and 4
        C=copy(C_temp)
    end
    return C[1]
end
	
"""
calc_pauli_rho(B, p, psite)

Calculate the expectation of a Pauli matrix on given site, Tr(rho*sigma^p_site), of rho stored as right-canonical MPS B.
"""
function calc_pauli_rho(B, p, psite)
    C=ones(1) # Contracted matrix
    for site in 1:L
        B_mpo=reshape(B[site],2,2,size(B[site],2),size(B[site],3))
        if site==psite
            @tensoropt C_temp[a2] := C[a1] * B_mpo[s1,s1p,a1,a2] * pauli[p][s1p,s1]
        else
            @tensoropt C_temp[a2] := C[a1] * B_mpo[s1,s1,a1,a2]
        end 
        C=copy(C_temp)
    end
    return real(C[1])
end

"""
    calc_autocorr(B_rho,B_heis)
    
    Calculate the 2-time autocorrelation function C(t,Dt)=<sz(t+Dt) sz(t)> for sz on site 1 using the quantum regression thm,
    C(t,Dt)=Tr[exp(L^\dagger*Dt)[sz]*sz*exp(L t)*rho].
    B_rho is assumed to store rho(t)=exp(L t)*rho and B_heis is assumed to store exp(L^\dagger*Dt)[sz] in MPS forms.
"""
function calc_autocorr(B_rho,B_heis)
    C=ones(1,1) # Contracted matrix
    weight=Diagonal(ones(2))
    weight[2,2]=-1 # This will be the reweighting of spin parts by sigma_z on site 1, immediately reset after first step
    for site in 1:L
        B_rho_mpo=reshape(B_rho[site],2,2,size(B_rho[site],2),size(B_rho[site],3))
        B_heis_mpo=reshape(B_heis[site],2,2,size(B_heis[site],2),size(B_heis[site],3))
        
        @tensoropt C_temp[a2,a2p] := C[a1,a1p] * B_rho_mpo[s1,s1p,a1,a2] * B_heis_mpo[s1p,s1pp,a1p,a2p] * weight[s1,s1pp]

        C=copy(C_temp)
        if site==1
            weight[2,2]=1 # For everything besides site 1, the sz(t) operator is just the identity matrix
        end
    end
    return real(C[1,1])
end

function main()
    
    # Read parameters from file
    include("params_tebd_ising.jl")

    # Using Forest-Ruth formula for 4th order Trotter from notes by Feigun.
    theta=1/(2-2^(1/3))
    dt1=dt*theta/2
    dt2=dt*theta
    dt3=dt*(1-theta)/2
    dt4=dt*(1-2*theta)
    
    # Initialize wfn as MPS
    Lam_psi = []
    B_psi = []
    Lam_psi0=ones(1,1)
    B_psi0=zeros(2,1,1)
    if init_dir=='x'
        # Start with everything pointing to the right, spin wfn (1/sqrt(2),1/sqrt(2))
        B_psi0[1,1,1]=1/sqrt(2) 
        B_psi0[2,1,1]=1/sqrt(2)
    elseif init_dir=='z'
        # Start with everything pointing up, spin wfn (1,0)
        B_psi0[1,1,1]=1
    else
        println("Invalid initial direction: ",init_dir)
        exit()
    end

    for i in 1:L
        if i < L
	    push!(Lam_psi,copy(Lam_psi0))
        end
	push!(B_psi,copy(B_psi0))
    end
    
    h2_left = (L == 2) ? h : h/2 # If L==2, then the only thing we ever see is V_left

    # First do imaginary time evolution
    # For real time evolution, just replace make_imag_time_ev -> make_unitary
    
    V1_bulk = make_imag_time_ev(jz,h/2,h/2,dt1)
    V1_left = make_imag_time_ev(jz,h,h2_left,dt1)
    V1_right = make_imag_time_ev(jz,h/2,h,dt1)

    V2_bulk = make_imag_time_ev(jz,h/2,h/2,dt2)
    V2_left = make_imag_time_ev(jz,h,h2_left,dt2)
    V2_right = make_imag_time_ev(jz,h/2,h,dt2)

    V3_bulk = make_imag_time_ev(jz,h/2,h/2,dt3)
    V3_left = make_imag_time_ev(jz,h,h2_left,dt3)
    V3_right = make_imag_time_ev(jz,h/2,h,dt3)

    V4_bulk = make_imag_time_ev(jz,h/2,h/2,dt4)
    V4_left = make_imag_time_ev(jz,h,h2_left,dt4)
    V4_right = make_imag_time_ev(jz,h/2,h,dt4)

    println("Starting imaginary time evolution")
    start = time()

    norm_lst=zeros(ComplexF64,n_steps_imag_time_ev+1)
    sx_lst=zeros(L,n_steps_imag_time_ev+1)
    sz_lst=zeros(L,n_steps_imag_time_ev+1)
    t_lst=collect(0:dt:(n_steps_imag_time_ev+0.5)*dt)

    norm_lst[1]=calc_norm(B_psi)
    for site in 1:L
        sx_lst[site,1]=calc_pauli(B_psi,1,site)
        sz_lst[site,1]=calc_pauli(B_psi,3,site)
    end

    for k in 1:n_steps_imag_time_ev
	println("At time step ",k)

        # exp((even+odd)*dt) = exp(even*dt1) exp(odd*dt2) exp(even*dt3) exp(odd*dt4) exp(even*dt3) exp(odd*dt2) exp(even*dt1)
        tedb!(B_psi,Lam_psi,V1_bulk,V1_left,V1_right,chi,epsilon,0)
        tedb!(B_psi,Lam_psi,V2_bulk,V2_left,V2_right,chi,epsilon,1)
        tedb!(B_psi,Lam_psi,V3_bulk,V3_left,V3_right,chi,epsilon,0)
        tedb!(B_psi,Lam_psi,V4_bulk,V4_left,V4_right,chi,epsilon,1)
        tedb!(B_psi,Lam_psi,V3_bulk,V3_left,V3_right,chi,epsilon,0)
        tedb!(B_psi,Lam_psi,V2_bulk,V2_left,V2_right,chi,epsilon,1)
        tedb!(B_psi,Lam_psi,V1_bulk,V1_left,V1_right,chi,epsilon,0)

        norm_lst[k+1]=calc_norm(B_psi)
        for site in 1:L
            sx_lst[site,k+1]=calc_pauli(B_psi,1,site)
            sz_lst[site,k+1]=calc_pauli(B_psi,3,site)
        end
    end

    stop = time()
    println("Total time = ",stop-start)

    npzwrite("t_prep.npy",t_lst)
    npzwrite("norm_prep.npy",real.(norm_lst))
    npzwrite("sx_prep.npy",sx_lst)
    npzwrite("sz_prep.npy",sz_lst)

    # Heisenberg time evolution of boundary spin

    # Setup initial operator as sigma_z on site 1
    Lam_heis = []
    B_heis = []
    Lam_heis0=ones(1,1)
    sz_heis0=reshape(pauli[3],4,1,1)
    eye_heis0=reshape(pauli[4],4,1,1)
    for i in 1:L
        if i < L
	    push!(Lam_heis,copy(Lam_heis0))
        end
        if i > 1
	    push!(B_heis,copy(eye_heis0))
        else
	    push!(B_heis,copy(sz_heis0))
        end
    end

    B_heis_all=[] # Save B-matrices for each output step for later calculation of 2-time sz-sz correlation function

    # For TFI with L=sigma_z, Heisenberg evolution is given simply by H->-H, L=L^\dagger unchanged
    U1_bulk = make_lindblad_time_ev(jz,-h/2,-h/2,0,dt1) 
    U1_left = make_lindblad_time_ev(jz,-h,-h2_left,gamma,dt1)
    U1_right = make_lindblad_time_ev(jz,-h/2,-h,0,dt1)

    U2_bulk = make_lindblad_time_ev(jz,-h/2,-h/2,0,dt2)
    U2_left = make_lindblad_time_ev(jz,-h,-h2_left,gamma,dt2)
    U2_right = make_lindblad_time_ev(jz,-h/2,-h,0,dt2)

    U3_bulk = make_lindblad_time_ev(jz,-h/2,-h/2,0,dt3)
    U3_left = make_lindblad_time_ev(jz,-h,-h2_left,gamma,dt3)
    U3_right = make_lindblad_time_ev(jz,-h/2,-h,0,dt3)

    U4_bulk = make_lindblad_time_ev(jz,-h/2,-h/2,0,dt4)
    U4_left = make_lindblad_time_ev(jz,-h,-h2_left,gamma,dt4)
    U4_right = make_lindblad_time_ev(jz,-h/2,-h,0,dt4)

    println("Starting Heisenberg time evolution")
    start = time()
    
    push!(B_heis_all,deepcopy(B_heis)) # Store initial state

    if !isapprox(round(dt_heis_out/dt),dt_heis_out/dt)
        println("Error: dt_heis_out must be an integer multiple of dt")
        println("dt_heis_out=",dt_heis_out)
        println("dt=",dt)
        exit()
    end
    n_dt_per_out=Int64(round(dt_heis_out/dt))

    t_heis_out=collect(0:dt_heis_out:(t_max_heis+dt/2))
    n_output_steps=length(t_heis_out)

    t_max_heis_real=t_heis_out[end]

    n_steps_heis_time_ev = Int64(round(t_max_heis_real/dt))
    for k in 1:n_steps_heis_time_ev
	println("At time step ",k)

        # exp((even+odd)*dt) = exp(even*dt1) exp(odd*dt2) exp(even*dt3) exp(odd*dt4) exp(even*dt3) exp(odd*dt2) exp(even*dt1)
        # DO NOT RENORMALIZE OPERATOR IN HEISENBERG PICTURE, BECAUSE NOT TRUE THAT TRACE IS 1
        # Loss of weight is inevitable, but should have little effect because that's precisely what the dissipative
        # part of Lindblad evolution is designed to do
        tedb!(B_heis,Lam_heis,U1_bulk,U1_left,U1_right,chi,epsilon,0,false)
        tedb!(B_heis,Lam_heis,U2_bulk,U2_left,U2_right,chi,epsilon,1,false)
        tedb!(B_heis,Lam_heis,U3_bulk,U3_left,U3_right,chi,epsilon,0,false)
        tedb!(B_heis,Lam_heis,U4_bulk,U4_left,U4_right,chi,epsilon,1,false)
        tedb!(B_heis,Lam_heis,U3_bulk,U3_left,U3_right,chi,epsilon,0,false)
        tedb!(B_heis,Lam_heis,U2_bulk,U2_left,U2_right,chi,epsilon,1,false)
        tedb!(B_heis,Lam_heis,U1_bulk,U1_left,U1_right,chi,epsilon,0,false)

        if k % n_dt_per_out == 0
            push!(B_heis_all,deepcopy(B_heis))
        end
    end
    @assert length(B_heis_all)==n_output_steps
    
    println("Total time = ",stop-start)

    npzwrite("t_heis.npy",t_heis_out)

    # Lindblad time evolution of density matrix (Schrodinger picture)

    U1_bulk = make_lindblad_time_ev(jz,h/2,h/2,0,dt1)
    U1_left = make_lindblad_time_ev(jz,h,h2_left,gamma,dt1)
    U1_right = make_lindblad_time_ev(jz,h/2,h,0,dt1)

    U2_bulk = make_lindblad_time_ev(jz,h/2,h/2,0,dt2)
    U2_left = make_lindblad_time_ev(jz,h,h2_left,gamma,dt2)
    U2_right = make_lindblad_time_ev(jz,h/2,h,0,dt2)

    U3_bulk = make_lindblad_time_ev(jz,h/2,h/2,0,dt3)
    U3_left = make_lindblad_time_ev(jz,h,h2_left,gamma,dt3)
    U3_right = make_lindblad_time_ev(jz,h/2,h,0,dt3)

    U4_bulk = make_lindblad_time_ev(jz,h/2,h/2,0,dt4)
    U4_left = make_lindblad_time_ev(jz,h,h2_left,gamma,dt4)
    U4_right = make_lindblad_time_ev(jz,h/2,h,0,dt4)

    (B_rho,Lam_rho)=make_rho_mps_from_psi_mps(B_psi,Lam_psi)


    println("Starting time evolution")
    start = time()

    mps_norm_lst=zeros(ComplexF64,n_steps_time_ev+1)
    mpo_norm_lst=zeros(ComplexF64,n_steps_time_ev+1)
    sx_lst=zeros(L,n_steps_time_ev+1)
    sz_lst=zeros(L,n_steps_time_ev+1)
    C_lst=zeros(length(t_heis_out),n_steps_time_ev+1) # sz-sz autocorrelation function of boundary spin
    t_lst=collect(0:dt:(n_steps_time_ev+0.5)*dt)

    mps_norm_lst[1]=calc_norm(B_rho) # Check that MPS stays right-normalized
    mpo_norm_lst[1]=calc_norm_rho(B_rho) # Find tr(rho), which generally will not be 1
    for site in 1:L
        sx_lst[site,1]=calc_pauli_rho(B_rho,1,site)/real(mpo_norm_lst[1])
        sz_lst[site,1]=calc_pauli_rho(B_rho,3,site)/real(mpo_norm_lst[1])
    end
    for j in 1:length(t_heis_out)
        C_lst[j,1]=calc_autocorr(B_rho,B_heis_all[j])/real(mpo_norm_lst[1])
    end 

    for k in 1:n_steps_time_ev
	println("At time step ",k)

        # exp((even+odd)*dt) = exp(even*dt1) exp(odd*dt2) exp(even*dt3) exp(odd*dt4) exp(even*dt3) exp(odd*dt2) exp(even*dt1)
        tedb!(B_rho,Lam_rho,U1_bulk,U1_left,U1_right,chi,epsilon,0)
        tedb!(B_rho,Lam_rho,U2_bulk,U2_left,U2_right,chi,epsilon,1)
        tedb!(B_rho,Lam_rho,U3_bulk,U3_left,U3_right,chi,epsilon,0)
        tedb!(B_rho,Lam_rho,U4_bulk,U4_left,U4_right,chi,epsilon,1)
        tedb!(B_rho,Lam_rho,U3_bulk,U3_left,U3_right,chi,epsilon,0)
        tedb!(B_rho,Lam_rho,U2_bulk,U2_left,U2_right,chi,epsilon,1)
        tedb!(B_rho,Lam_rho,U1_bulk,U1_left,U1_right,chi,epsilon,0)

        mps_norm_lst[k+1]=calc_norm(B_rho) # Check whether MPS stays right-normalized, not generally the case with dissipative Liouvillian
        mpo_norm_lst[k+1]=calc_norm_rho(B_rho) # Find tr(rho), which generally will not be 1
        for site in 1:L
            sx_lst[site,k+1]=calc_pauli_rho(B_rho,1,site)/real(mpo_norm_lst[k+1])
            sz_lst[site,k+1]=calc_pauli_rho(B_rho,3,site)/real(mpo_norm_lst[k+1])
        end
        for j in 1:length(t_heis_out)
            C_lst[j,k+1]=calc_autocorr(B_rho,B_heis_all[j])/real(mpo_norm_lst[k+1])
        end 
    end

    stop = time()
    println("Total time = ",stop-start)

    npzwrite("t.npy",t_lst)
    npzwrite("Dt.npy",t_heis_out)
    npzwrite("mps_norm.npy",real.(mps_norm_lst))
    npzwrite("mpo_norm.npy",real.(mpo_norm_lst))
    npzwrite("sx.npy",sx_lst)
    npzwrite("sz.npy",sz_lst)
    npzwrite("C.npy",C_lst)

end

main()
