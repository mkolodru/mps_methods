# Find time evolution of TFI chain using TEBD
# Using notations from Schollwock review, arXiv:1008.3477v2
# Instead of working directly with density matrix, unfold as ket followed by bra. This will be useful for boundary dissipation.

using LinearAlgebra
using TensorOperations
using NPZ

pauli=[[0 1;1 0],[0 -im;im 0],[1 0;0 -1],[1 0;0 1]]

"""
make_time_ev(J_z,h_1,h_2,gamma,dt)

Create (non-unitary) evolution operator U = exp(LL*dt) for TEBD of the 2-site form LL=-i*H + lind, with
Hamiltonian H=-J_z*z1*z2 - h_1*x1 - h_2*x2 and lindblad component lind=gamma*(z1*z2-1)
Return the unitary as a 2x2x2x2 tensor in the form needed for TEBD, U[s1,s2,s1',s2']
"""
function make_time_ev(J_z,h_1,h_2,gamma,dt)
    @tensor twositeop[s1,s2,s1p,s2p] := (-im*(-J_z*pauli[3][s1,s1p]*pauli[3][s2,s2p] - h_1*pauli[1][s1,s1p]*pauli[4][s2,s2p] 
                                              -h_2*pauli[4][s1,s1p]*pauli[1][s2,s2p])
                                         + gamma*(pauli[3][s1,s1p]*pauli[3][s2,s2p] - pauli[4][s1,s1p]*pauli[4][s2,s2p]))

    return reshape(exp(dt*reshape(twositeop,(4,4))),(2,2,2,2)) # u[s1,s2,s1',s2']*psi[s1',s2']=psif[s1,s2]
end

"""
make_right_canon!(mps,Lam)

Do single sweep on mps from right to left to do right-canonicalization. Update singular value matrices Lam accordingly.
"""
function make_right_canon!(mps,Lam)
    resize!(Lam,0)
    for site in length(mps):-1:1
        # Reshape [s2,a1,a2] -> [a1,(s2,a2)]
        M=mps[site] # Alias
        (U,S,V_dag)=svd_and_trim!(reshape(permutedims(M,(2,1,3)),size(M,2),size(M,1)*size(M,3)),0,0)
        if site > 1
            push!(Lam,Diagonal(S))
        end
        
        # Update mps[site]
        # Reshape [a1,(s2,a2)] -> [s2,a1,a2]
        mps[site]=permutedims(reshape(V_dag,size(V_dag,1),size(M,1),size(M,3)),(2,1,3))

        if site > 1
            M=copy(mps[site-1])
            @tensoropt mps[site-1][s2,a1,a2]:=M[s2,a1,a2pp]*U[a2pp,a2p]*Diagonal(S)[a2p,a2]
        end
    end # for loop over sites
    reverse!(Lam)
end # function make_right_canon!

"""
make_rho_mps_from_psi_mps(B_psi, Lam_psi)

Create MPS corresponding to density matrix rho = |psi><psi| in TEBD form (right-normd B matrices and diagonal Lambda matrices).
For this code, rho will be unfolded as the bra followed by the ket.
"""
function make_rho_mps_from_psi_mps(B_psi)
    B_rho=[]
    Lam_rho=[]
    L=length(B_psi)
    for site in L:-1:1
        @tensor B_temp[s,a,b]:=conj(B_psi[site][s,b,a]) # Take dagger of each matrix, reverse order
        push!(B_rho,deepcopy(B_temp))
    end
    append!(B_rho,deepcopy(B_psi))

    make_right_canon!(B_rho,Lam_rho)
    
    return (B_rho,Lam_rho)
end

"""
svd_and_trim!(m,epsilon,chi)

Do SVD m = a * Diagonal(s) * v', cut to min of error epsilon on sqrt(sum(s^2)) or max bond dim chi.
To conserve memory, use svd!, meaning that m is trash after running it.

Note that if it is run with chi=0, will not trim.
"""
function svd_and_trim!(m,epsilon,chi)
    F=svd!(m) # m is now garbage
    a=F.U
    s=F.S
    vdag=F.Vt

    if chi==0
        return (a,s,vdag)
    end
    
    error = 0
    dim = 1
    for k in 0:(length(s)-1)
	error += s[end-k]^2
	if sqrt(error) > epsilon
	    dim = length(s)-k
	    break
        end
    end
    
    dim = min(dim,chi)
    
    return (a[:,1:dim], s[1:dim], vdag[1:dim,:])
end

"""
tedb!(B,Lam,U_lst,chi,epsilon,bond,renorm=true)

Do TEBD on density matrix stored as site matrices B, bond matrices Lam, list U describing
time evolution on each bond, max bond dimension chi, max error epsilon. bond=0 or 1 specifies even or odd bonds.

For bond=0, U=[U(site 1&2), U(site 3&4), ...]
For bond=1, U=[U(site 2&3), U(site 4&5), ...]

If renorm is set to false, does not divide by sum of squared singular values (norm) on truncation.
"""
function tedb!(B,Lam,U,chi,epsilon,bond;renorm=true)
    L=length(B) # Number of sites
    startsite = (bond == 0) ? 1 : 2
    endsite = L-1
    one_tensor=ones(1,1) # For use at the edge, 2-dimensional tensor Lam_0 = Lam_L = 1.
    a=nothing
    s=nothing
    vdag=nothing
    
    # unitary evolution on all even/odd bonds
    #    U_cnt=1 # Counter to keep track of current bond in U
    @assert length(startsite:2:endsite) == length(U) # Make sure I've been giving appropriate unitaries
    for i in startsite:2:endsite
        # Pick the correct unitary for evolution (as alias to U_curr)
        U_curr=U[div(i-startsite,2)+1]

        # Lam[i-1] = L_0 iff i=1
        Lam_prev=(i==1) ? one_tensor : Lam[i-1]

        # Do TEBD evolution under U_curr, using notation from Schollwock review
        @tensor psi_bar[s1,s2,a0,a2]:=B[i][s1,a0,a1]*B[i+1][s2,a1,a2]
	@tensor phi_bar[s1,s2,a0,a2] := U_curr[s1,s2,s1p,s2p]*psi_bar[s1p,s2p,a0,a2]
	@tensor phi[a0,s1,a2,s2] := Lam_prev[a0,a1]*phi_bar[s1,s2,a1,a2]
        phi=reshape(phi,size(phi,1)*size(phi,2),size(phi,3)*size(phi,4)) # Regroup as phi[(a0,s1),(a2,s2)]

	(a,s,vdag) = svd_and_trim!(phi,epsilon,chi) # phi is now garbage

        norm = renorm ? sqrt(sum(s.^2)) : 1
	Lam[i] = Diagonal(s)./norm # Stored as sparse diagonal matrix for simplicity
	
        B[i+1] = permutedims(reshape(vdag,length(s),size(phi_bar,4),size(phi_bar,1)),(3,1,2)) # reshape from [a1,(a2,s2)] to [a1,a2,s2], then permute to [s2,a1,a2]
	
	@tensor B[i][s1,a0,a1] := phi_bar[s1,s2,a0,a2]*conj(B[i+1][s2,a1,a2])/norm

        #        U_cnt=U_cnt+1
    end # for i in startsite:2:endsite
end # function tebd

"""
calc_norm(B)

Calculate the norm, <psi|psi>, of wfn psi stores as right-canonical MPS B.
"""
function calc_norm(B)
    # Contract from left to right (shouldn't matter, but note that this is oppose the canonical form
    C=ones(1,1) # Contracted matrix
    for site in 1:L
        B_curr=B[site]
        @tensoropt C_temp[a2,a2p] := C[a1,a1p] * B_curr[s1,a1,a2] * conj(B_curr[s1,a1p,a2p])
        C=copy(C_temp)
    end
    return C[1,1]
end

"""
calc_op_trace(B)

Calculate the trace of operator stored as MPS B.
"""
function calc_op_trace(B)
    @assert length(B)%2==0
    L=div(length(B),2)
    B_bra=B[L]
    B_ket=B[L+1]
    @tensor C_temp[a2,b2] := B_bra[s,a2,a1]*B_ket[s,a1,b2]
    C=copy(C_temp)
    for site in 2:L
        B_bra=B[L-site+1]
        B_ket=B[L+site]
        try
            @tensoropt C_temp[a2,b2] := B_bra[s,a2,a1] * C[a1,b1] * B_ket[s,b1,b2] # Matrix multiplication, sum over s
        catch e
            println("site=",site)
            println("L=",L)
            println("size(B_bra)=",size(B_bra))
            println("size(B_ket)=",size(B_ket))
            println("size(C)=",size(C))
            println(e)
            exit()
        end
        C=copy(C_temp)
    end
    return C[1,1]
end

"""
apply_pauli_from_left!(B,site,p)

Replace MPS B -> P*B where P is pauli[p] acting on site site
"""
function apply_pauli_from_left!(B,site,p)
    @assert length(B) % 2 == 0
    L=div(length(B),2)
    @tensor B_site_temp[s2,a,b] := pauli[p][s2,s1] * B[L+site][s1,a,b]
    B[L+site]=copy(B_site_temp)
end

"""
calc_pauli_site(B,site,p)

Calculate tr(pauli[p] * rho), where B is MPS version of rho. Should end up with B unchanged.
"""
function calc_pauli_site(B,site,p)
    apply_pauli_from_left!(B,site,p) # B_rho -> s[p]*B_rho
    val=calc_op_trace(B)
    apply_pauli_from_left!(B,site,p) # s[p]*B_rho -> s[p]^2*B_rho = B_rho
    return val
end

function main()
    
    include("params_tebd_ising_boundary_diss.jl") # Read parameters from file

    # Using Forest-Ruth formula for 4th order Trotter from notes by Feigun.
    theta=1/(2-2^(1/3))
    dt1=dt*theta/2
    dt2=dt*theta
    dt3=dt*(1-theta)/2
    dt4=dt*(1-2*theta)

    B_psi=[]

    # Start by reading from output of DMRG
    for site in 1:L
        push!(B_psi,npzread("mps_"*string(site)*".npy"))
    end
    
    # Start with product state
    # B_psi0=zeros(2,1,1)
    # # Start out pointing up
    # #    B_psi0[1,1,1]=1  
    # # Start with everything pointing to the right, spin wfn (1/sqrt(2),1/sqrt(2))
    # B_psi0[1,1,1]=1/sqrt(2) 
    # B_psi0[2,1,1]=1/sqrt(2)
    # for site in 1:L
    #     push!(B_psi,copy(B_psi0))
    # end

    (B_rho, Lam_rho) = make_rho_mps_from_psi_mps(B_psi)

    # Create time evolution matrices
    U1_even=[]
    U3_even=[]
    U2_odd=[]
    U4_odd=[]

    for bond in 1:(2*L-1)
        for dt_cnt in 1:2
            # Setup aliases to append to correct list
            U=nothing
            dt=nothing
            if (bond-1) % 2 == 0 # Even bond
                if dt_cnt==1
                    U=U1_even
                    dt=dt1
                else
                    U=U3_even
                    dt=dt3
                end
            else
                if dt_cnt==1
                    U=U2_odd
                    dt=dt2
                else
                    U=U4_odd
                    dt=dt4
                end
            end

            # Now determine the matrix
            if bond < L # bra, H -> -H
                if bond==1 # "Left" edge
                    push!(U,make_time_ev(-J_z,-h,-h/2,0,dt))
                else # Bulk
                    push!(U,make_time_ev(-J_z,-h/2,-h/2,0,dt))
                end
            elseif bond > L # ket
                if bond==2*L-1 # "Right" edge
                    push!(U,make_time_ev(J_z,h/2,h,0,dt))
                else # Bulk
                    push!(U,make_time_ev(J_z,h/2,h/2,0,dt))
                end
            else # dissipative edge, incorporate h-field on neighboring sites
                push!(U,make_time_ev(0,-h/2,h/2,gamma,dt))
            end
        end
    end

    # Determine information on time evolution of sz*rho(t) for auto-correlation function
    if !isapprox(round(Dt_step_corr_out/dt),Dt_step_corr_out/dt)
        println("Error: Dt_step_corr_out must be an integer multiple of dt")
        println("Dt_step_corr_out=",Dt_step_corr_out)
        println("dt=",dt)
        exit()
    end
    n_dt_per_Dt_out=Int64(round(Dt_step_corr_out/dt))
    Dt_out=collect(0:Dt_step_corr_out:(Dt_max+dt/2))
    n_steps_corr_time_ev = Int64(round(Dt_out[end]/dt))
    
    if !isapprox(round(t_step_corr_out/dt),t_step_corr_out/dt)
        println("Error: t_step_corr_out must be an integer multiple of dt")
        println("t_step_corr_out=",t_step_corr_out)
        println("dt=",dt)
        exit()
    end
    n_dt_per_t_out=Int64(round(t_step_corr_out/dt))
    t_corr_out=collect(0:t_step_corr_out:(t_max+dt/2)) # Time values for output of C(t,Dt)
    
    # Prepare lists for output
    mps_norm_lst=zeros(ComplexF64,n_steps_time_ev+1)
    mpo_norm_lst=zeros(ComplexF64,n_steps_time_ev+1) # Equal to tr(rho)
    sx_lst=zeros(L,n_steps_time_ev+1)
    sz_lst=zeros(L,n_steps_time_ev+1)
    C_lst=zeros(ComplexF64,length(Dt_out),length(t_corr_out)) # sz-sz autocorrelation function of boundary spin
    t_lst=collect(0:dt:(n_steps_time_ev+0.5)*dt)

    println("Starting time evolution")
    start = time()

    for j in 1:(n_steps_time_ev+1)
        println("At time step ",j,"/",n_steps_time_ev+1," of outer loop")

        if j > 1
            # exp((even+odd)*dt) = exp(even*dt1) exp(odd*dt2) exp(even*dt3) exp(odd*dt4) exp(even*dt3) exp(odd*dt2) exp(even*dt1)
            tedb!(B_rho,Lam_rho,U1_even,chi,epsilon,0)
            tedb!(B_rho,Lam_rho,U2_odd,chi,epsilon,1)
            tedb!(B_rho,Lam_rho,U3_even,chi,epsilon,0)
            tedb!(B_rho,Lam_rho,U4_odd,chi,epsilon,1)
            tedb!(B_rho,Lam_rho,U3_even,chi,epsilon,0)
            tedb!(B_rho,Lam_rho,U2_odd,chi,epsilon,1)
            tedb!(B_rho,Lam_rho,U1_even,chi,epsilon,0)
        end

        mps_norm_lst[j]=calc_norm(B_rho) # Check that MPS stays right-normalized (currently it doesn't, due to non-hermitian H_eff in unfolded picture)
        mpo_norm_lst[j]=calc_op_trace(B_rho) # Find tr(rho), which generally will not be 1
        for site in 1:L
            sx_lst[site,j]=real(calc_pauli_site(B_rho,site,1))/real(mpo_norm_lst[j]) 
            sz_lst[site,j]=real(calc_pauli_site(B_rho,site,3))/real(mpo_norm_lst[j]) 
        end

        if (j-1) % n_dt_per_t_out != 0
            continue
        end

        # Time evolve in inner loop over Dt to calculate 2-time correlation fn <sz(t+Dt) sz(t)>
        t_ind=div(j-1,n_dt_per_t_out)+1
        println("Doing inner loop to evaluate corr fn, j=",j,", t_ind=",t_ind)
        B_corr=deepcopy(B_rho)
        Lam_corr=deepcopy(Lam_rho)
        apply_pauli_from_left!(B_corr,1,3) # rho_corr(t) = sz_1 * rho(t)
        
        for k in 1:(n_steps_corr_time_ev+1)
            println("\tAt time step ",k,"/",n_steps_corr_time_ev+1," of inner loop")

            if k > 1
                # exp((even+odd)*dt) = exp(even*dt1) exp(odd*dt2) exp(even*dt3) exp(odd*dt4) exp(even*dt3) exp(odd*dt2) exp(even*dt1)
                # DO NOT RENORMALIZE OPERATOR DURING THIS EVOLUTION, BECAUSE TR(OP) IS NO LONGER EQUAL TO 1
                # Loss of weight is inevitable, but should have little effect because that's precisely what the dissipative
                # part of Lindblad evolution is designed to do
                tedb!(B_corr,Lam_corr,U1_even,chi,epsilon,0; renorm=false)
                tedb!(B_corr,Lam_corr,U2_odd,chi,epsilon,1; renorm=false)
                tedb!(B_corr,Lam_corr,U3_even,chi,epsilon,0; renorm=false)
                tedb!(B_corr,Lam_corr,U4_odd,chi,epsilon,1; renorm=false)
                tedb!(B_corr,Lam_corr,U3_even,chi,epsilon,0; renorm=false)
                tedb!(B_corr,Lam_corr,U2_odd,chi,epsilon,1; renorm=false)
                tedb!(B_corr,Lam_corr,U1_even,chi,epsilon,0; renorm=false)
            end

            if (k-1) % n_dt_per_Dt_out == 0
                C_lst[div(k-1,n_dt_per_Dt_out)+1,t_ind]=calc_pauli_site(B_corr,1,3)/real(mpo_norm_lst[j])
            end
        end
    end

    stop = time()
    println("Total time = ",stop-start)

    npzwrite("t.npy",t_lst)
    npzwrite("Dt_corr.npy",Dt_out)
    npzwrite("t_corr.npy",t_corr_out)
    npzwrite("mps_norm.npy",real.(mps_norm_lst))
    npzwrite("mpo_norm.npy",real.(mpo_norm_lst))
    npzwrite("sx.npy",sx_lst)
    npzwrite("sz.npy",sz_lst)
    npzwrite("C.npy",C_lst)

end

main()
