L = 100
J_z = 1
h = 2
chi = 50
epsilon = 1e-7
dt = 0.1
t_step_corr_out=2 # For output of C(t,Dt), t = (0,1,2,...)*t_step_corr_out (code will force to be a multiple of dt)
Dt_step_corr_out=0.1 # For output of C(t,Dt), Dt = (0,1,2,...)*Dt_step_corr_out (code will force to be a multiple of dt)
t_max = 10
Dt_max = 10 # For time evolution of sz*rho to measure 2-time correlation function
gamma = 1.5
n_steps_time_ev = Int64(round(t_max/dt))

